---
layout: markdown_page
title: "Visual Studio Team Services (VSTS)/Team Foundation Server (TFS)/Azure DevOps"
---
<!-- This is the template for sections to include and the order to include
them. If a section has no content yet then leave it out. Leave this note in
tact so that others can see where new sections should be added.

### Summary
### Strengths
### Challenges
### Who buys and why
### Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
### Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
### FAQs
 - about the product  <-- comment. delete this line
### Integrations
### Pricing
   - summary, links to tool website  <-- comment. delete this line
### Comparison to GitLab
  #### Features
     - link to comparison page  <-- comment. delete this line
  #### Value/ROI
     - link to ROI calc?  <-- comment. delete this line
### Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

<!-- ## On this page
{:.no_toc}
-->

- TOC
{:toc}

## Summary
On September 10, 2018 Microsoft renamed VSTS to Azure DevOps and TFS to Azure DevOps Server. and upgraded both with the same new user interface.

Visual Studio Team Services (VSTS) is a hosted cloud offering, and Team Foundation Server (TFS), is an on-premises version. Essentially, VSTS is a cloud, hosted version of TFS. Both offer functionality that cover multiple stages of the DevOps lifecycle including planning tools, source code managment (SCM), and CI/CD.

As part of their SCM functionality, both platforms offer two methods of version control.

1. [Git](https://www.visualstudio.com/team-services/git/) (distributed) - each developer has a copy on their dev machine of the source repository including all branch and history information.

2. Team Foundation Version Control ([TFVC](https://www.visualstudio.com/team-services/tfvc/)), a centralized, client-server system - developers have only one version of each file on their dev machines. Historical data is maintained only on the server.

Microsoft recommends customers use Git for version control unless there is a specific need for centralized version control features. [https://docs.microsoft.com/en-us/vsts/tfvc/comparison-git-tfvc](https://docs.microsoft.com/en-us/vsts/tfvc/comparison-git-tfvc)

This is noteworthy given that in June of 2018 Microsoft purchased [GitHub](../github/), the Internets largest online code repository.

## Resources
- [Visual Studio Team Services](https://www.visualstudio.com/team-services/)
- [Team Foundation Server](https://www.visualstudio.com/tfs/))
- [Azure DevOps](https://azure.microsoft.com/en-us/services/devops/)
- [Azure DevOps Announcement Blog](https://azure.microsoft.com/en-us/blog/introducing-azure-devops/)
- [Azure DevOps public Roadmap and Release History](https://docs.microsoft.com/en-us/azure/devops/release-notes/)

## Comments/Anecdotes
* Lots fo emphasis on cross platform (windows, Mac, Linux), and free macOS CI/CD is pretty rare.
* From [https://azure.microsoft.com/en-us/blog/introducing-azure-devops/](https://azure.microsoft.com/en-us/blog/introducing-azure-devops/)
   > Azure DevOps represents the evolution of Visual Studio Team Services (VSTS). VSTS users will be upgraded into Azure DevOps projects automatically. For existing users, there is no loss of functionally, simply more choice and control. The end to end traceability and integration that has been the hallmark of VSTS is all there. Azure DevOps services work great together.

   > As part of this change, the services have an updated user experience.

   > Users of the on-premises Team Foundation Server (TFS) will continue to receive updates based on features live in Azure DevOps. Starting with next version of TFS, the product will be called Azure DevOps Server and will continue to be enhanced through our normal cadence of updates.
* [HackerNews comments saying it's just a rebrand](https://news.ycombinator.com/item?id=17952273) - PM for AzureDevOps responding:
   > PM for Azure DevOps here (formerly VSTS). It is a rebranding, but it's more than merely a rebranding. We're breaking out the individual services so that they're easier to adopt. For example, if you're just interested in pipelines, you can adopt only pipelines.
* From a call with a prospect Bank:
   - Went with Azure DevOps because
      > It's platform agnostic, it's in the cloud, great capabilityality, tons of functionality, it does what we need it to do. We like it a lot. It really has nothing to do with Microsoft. Microsoft is very agnostic and open source embracing now, so that the old Java vs .Net thing is kind of over.

   - Appealed to a shop that was "more Java than Microsoft technologies". But they had lots of the Microsoft development suite already, and trusted where Microsoft is going.
   - Azure DevOps is dropping new releases every sprint (2-3 weeks). Their roadmap is public: [Azure DevOps public Roadmap and Release History](https://docs.microsoft.com/en-us/azure/devops/release-notes/)

## Pricing

### VSTS
{:.no_toc}

[VSTS Pricing](https://visualstudio.microsoft.com/team-services/pricing/)

Visual Studio ‘Professional Version’ is the most comparable to GitLab since Visual Studio ‘Enterprise Version’ includes extras outside the scope of DevOps (such as MS Office, etc).

Visual Studio Professional can be purchased under a ‘standard’ or ‘cloud’ model.

- Standard = $1,200 year one (retail pricing), then $800 annual renewals (retail pricing)
- Cloud - $540 per year

Under their ‘modern purchasing model’, the monthly cost for Visual Studio Professional (which includes TFS and CAL license) is $45 / mo ($540 / yr).  However, extensions to TFS such as [Test Manager](https://marketplace.visualstudio.com/items?itemName=ms.vss-testmanager-web) ($52/mo), [Package Management](https://marketplace.visualstudio.com/items?itemName=ms.feed) ($15/mo), and [Private Pipelines](https://marketplace.visualstudio.com/items?itemName=ms.build-release-private-pipelines) ($15/mo) require an additional purchase.

### TFS
{:.no_toc}

[TFS Pricing](https://visualstudio.microsoft.com/team-services/tfs-pricing/)

A TFS license can be purchased as standalone product, but a TFS license (and CAL license) is also included when you buy a Visual Studio license / subscription.

MS pushes Visual Studio subscriptions and refers customers who are only interested in a standalone TFS with a ‘classic purchasing’ model to license from a reseller.

Excluding CapEx and Windows operating system license, a standalone TFS license through a reseller in classic purchasing model is approximately $225 per year per instance.  The approximate Client Access License is approximately $320 per year.

### Azure DevOps
{:.no_toc}

[Azure DevOps Services Pricing](https://azure.microsoft.com/en-us/pricing/details/devops/azure-devops-services/)  
[Azure Pipelines Only Pricing](https://azure.microsoft.com/en-us/pricing/details/devops/azure-pipelines/)  
[Azure DevOps On-prem](https://azure.microsoft.com/en-us/pricing/details/devops/on-premises/) = See TFS Pricing  

## Comparison
