---
layout: markdown_page
title: "Building applications that meet HIPAA standards"
---
## See how using GitLab can help you with HIPAA compliance

The Health Insurance Portability and Accountability Act (HIPAA) sets the standard for sensitive patient data protection. 
Companies that deal with protected health information (PHI) must have physical, network, and process security measures in place and follow them to ensure HIPAA Compliance.

### [Rules](https://msdn.microsoft.com/en-us/library/aa480484.aspx#regcompliance_demystified_topic3) 

1. Administrative Safeguards must establish and enforce company privacy policies and procedures (for example, disaster recovery and contingency plans).
1. Physical Safeguards must encompass restrictions and rules that deal with physical access to facilities and machines, access controls, as well as the associated policies and procedures that deal with the physical entities in the organization.
1. Technical Standards contain all of the safeguards and practices that relate to the intangible information contained in the organizations computer systems such as intrusion prevention, data corroboration, and access controls. This paper will focus on the technical standards section because they contain most of the actionable items for developers.

### [Principles](https://msdn.microsoft.com/en-us/library/aa480484.aspx#regcompliance_demystified_topic3) 

1. Confidentiality: Design software so that the encryption algorithms are replaceable and key sizes can be easily increased so that encryption strength can keep pace with advances in computing power and cracking algorithms.
1. Integrity: Records should not be modifiable by unauthorized people or entities. 
1. Availability: Event logs should contain enough information to make it possible to reconstruct system activity up to the point of failure so that the error can be quickly resolved and fixed.
1. Auditing and Logging: Any actions that might need to be traced must be documented. 
1. Authentication: it is necessary to know that the entity or person that is working with the data is legitimate

### GitLab features

1. [Dependency scanning](https://docs.gitlab.com/ee/user/project/merge_requests/dependency_scanning.html#doc-nav) allows you to quickly find open source components with security vulnerabilities.
1. [Merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html#doc-nav) ensure that only authorized people modify software. 
1. [Audit event](https://docs.gitlab.com/ee/administration/audit_events.html#audit-events) helps you view changes made.
1. [Permissions](https://docs.gitlab.com/ee/user/permissions.html) 
1. [Self-managed](https://about.gitlab.com/2018/04/20/gitlab-tiers/#gitlab-self-hosted) so you can install in [Virtual Private Cloud](https://aws.amazon.com/vpc/) and have app and data under control.