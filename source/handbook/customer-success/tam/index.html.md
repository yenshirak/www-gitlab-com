---
layout: markdown_page
title: "Technical Account Management"
---

# Technical Account Management Handbook
{:.no_toc}

## On this page
{:.no_toc}

- TOC
{:toc}

## What is a Technical Account Manager (TAM)?

GitLab's Technical Account Managers serve as trusted advisors to GitLab customers. They offer guidance, planning and oversight during the technical deployment and implementation process. They fill a unique space in the overall service lifecycle and customer journey and actively bind together sales, solution architects, customer stakeholders, product management, implementation engineers and support. 

See the [Technical Account Manager role description](/job-families/sales/technical-account-manager/) for further information.

### Advocacy
A Technical Account Manager is an advocate for both the customer and GitLab. They act on behalf of customers serving as a feedback channel to development and shaping of the product.  In good balance, they also advocate on behalf of GitLab to champion capabilities and features that will improve quality, increase efficiency and realize new value for our customer base.

### Values

#### Management

Technical Account Managers maintain the relationships between the customers and GitLab. Making sure that everyone is working towards pre-defined goals and objectives.

#### Growth

Technical Account Managers help to bring GitLab to all aspects of your company, not just software development. They can do this by showing other business unit's how to use GitLab for their day-to-day tasks and to advocate for new features and functionality that are in demand by other groups.

#### Success

Technical Account Managers make sure that the adoption of GitLab is successful at your company through planning, implementation, adoption, training and regular healthchecks.

### Responsibilities and Services
All Premium customers with a minimum ARR of $100,000 are aligned with a Technical Account Manager. There are various services a Technical Account Manager will provide to ensure that customers get the best value possible out of their relationship with GitLab and their utilisation of GitLab's products and services. These services include, but are not limited to:

#### Relationship Management
* Regular cadence calls
* Regular open issue reviews and issue escalations
* Account healthchecks
* Quarterly business reviews
* Success strategy roadmaps - beginning with a 30/60/90 day success plan
* To act as a key point of contact for guidance, advice and as a liason between the customer and other GitLab teams
* Own, manage, and deliver the customer onboarding experience
* Help GitLab's customers realize the value of their investment in GitLab
* GitLab Days

#### Training
* Identification of pain points and training required
* Coordination of demos and training sessions, potentially delivered by the Technical Account Manager if time and technical knowledge allows
* "Brown Bag" trainings
* Regular communucation and updates on GitLab features
* Product and feature guidance - new feature presentations

#### Support
* Upgrade planning
* User adoption strategy
* Migration strategy and planning
* Launch support
* Monitors support tickets and ensures that the customer receives the appropriate support levels
* Support ticket escalations

It is also possible for a customer to pay for a Technical Account Manager's services in order to receive priority, "white glove" assistance, guidance and support as well as more time allocated to their account on a monthly basis. There are also additional services a Technical Account Manager will provide to the services listed above. These are currently to be determined and will become available before the end of the second quarter of 2018.

---

# Engagement Models
There are three models currently offered for Technical Account Manager engagement. These are broken into tiers that currently use Annual Recurring Revenue as a metric for determining a manageable volume for a single Technical Account Manager and the depth of involvement during the engagement.

## Table of Technical Account Manager Engagements
Technical Account Managers focus on [large and strategic accounts](/handbook/sales/#market-segmentation), [small business (SMB) and mid-market accounts](/handbook/sales/#market-segmentation) are typically handled by [Account Managers](/handbook/customer-success/account-management). 

||TIER 1 |TIER 2 |TIER 3 |
|:---|:---|:---|:---|
|Minimum ARR|$500,000|$250,000|$100,000|

## Managing the Customer Engagement
Technical Account Managers will typically manage customer engagements via a GitLab project in the [`account-management` group](https://gitlab.com/gitlab-com/account-management/). This project will be based off a [customer collaboration project template](https://gitlab.com/gitlab-com/account-management/customer-collaboration-project-template) and then customized to match the customer's needs as outlined above. The project is pre-loaded with milestones, issues, labels and a README template to help kick off the project and outline a proof of concept, implementation and customer onboarding.

### To start a new customer engagement:
1.  Somewhere between step 3 and step 7 of the customer journey sequence, a Solutions Architect should create a project for the customer in GitLab and include an Implementation Engineer and Technical Account Manager who're best aligned with the customer account.
2. Go to the [customer collaboration project template project](https://gitlab.com/gitlab-com/account-management/customer-collaboration-project-template).
3. Follow the steps in the PLEASE-READ-THESE-INSTRUCTIONS.md file

### Where does a Technical Account Manager fit in?
During the pre-sales process, a Solutions Architect owns the project with assistance from the Strategic Account Leader and should include the Implementation Engineer if there is one assigned. During the pre-sales project, a Technical Account Manager is involved but only for visibility. Until the account becomes a paying customer, the project remains in pre-sales. Once the customer has paid, the Strategic Account Leader will set up the Welcome to GitLab call along with the key GitLab employees (SAL, SA, IE and Technical Account Manager) and the customer. There is a preloaded issue for this in the project template.

This call will introduce the customer to the Technical Account Manager and begin the handover process. The Technical Account Manager will then lead the rest of the call and own the project going forward. The project is then moved from a pre-sales project under the [`pre-sales account-management` group](https://gitlab.com/gitlab-com/account-management/pre-sales) to a post-sales project under [`account-management` group](https://gitlab.com/gitlab-com/account-management).

Typically this project template isn't used for small business or mid-market accounts. However, if an Account Manager feels this could be useful to assist their relationship with the customer, then they can utilise it and edit down as needed. 

# Account Triaging

In order to prevent customer churn and improve retention of our customers, we have an [Account Triage](https://gitlab.com/gitlab-com/customer-success/account-triage) project in GitLab. 

## Account Triage Project

If customer at risk of non-renewal, i.e. with a health score of Amber or Red in SFDC, create an issue in the [Account Triage](https://gitlab.com/gitlab-com/customer-success/account-triage) project.

All TAM's meet weekly to review the issue board for current "at risk" account activity. The meeting is a peer review of new at risk accounts to make sure activities are followed up on and owned across the business and is also a chance to discuss tactics/strategies on those accounts in case anything different should be done. 

The triage team also needs to check the health score report to ensure that all Amber and Red accounts have indeed been added to the board, and to make sure that all issues in the project have the correct health score according to SFDC. If the scores do not match, reach out to the TAM to find out why and rectify it.

Generally, this account is for Amber and Red accounts. If a TAM has an account that is Yellow, and feels they need help and support on that account from their team / the triage team described below, then they may add an issue on the triage board for that account.

When an account is added to the issue board, assign it with a 'triage team', which should include: 
* Account Owner
* Technical Account Manager
* Manager of Customer Experience and/or Director of Customer Success
* Regional Sales Manager for Account
* Solutions Architect where appropriate
* Implementation Engineer where appropriate
* Executive Sponsor (probably the account owner's RD, but could be VP of Engineering for example, depending on the problems the customer is having)
* Product Manager(s) where appropriate

This team will have responsibility for managing the account as a team until renewal point.

The objective here is to try and retain customers, and reduce churn whilst balancing that with the demands of the rest of the business.  For example, if a customer is at risk over a feature that needs to be built, that cost on engineering/product time needs to be balanced with the commercial attractiveness of the customer.  For that reason it is a good idea for a representative of the product team to also attend the review meetings, as required.

Having such a process in place ensures that we are being proactive about managing our churn risk. It also means that we gain good visibility of potential churn in advance. If there are commonalities to churn risk customers, that will also help with visibility and prioritisation of any work required - or of any other problems.

If you move an account to Green or Yellow from Amber or Red, close it. Make sure you close it AND add the label so we can see the history in the issue.

This project/process is a work in progress and can always be improved. If you have suggestions on how to improve it, please leave add your thoughts to [this issue](https://gitlab.com/gitlab-com/customer-success/tam/issues/80).

## Labels:

#### Health scores (https://gitlab.com/gitlab-com/customer-success/cs-metrics/issues/6)

Rough guide below on what the health of an account in a certain category looks like:

~Red

* They’ve told us they are downgrading/cancelling.
* No communication.
* Limited or no access to executive sponsors.

~Amber

* Low user adoption.
* Lack of response to us and/ or we haven’t done much discovery.
* Complains about the company (responsiveness, feature turnaround, missed priorities in direction).
* Loss of our GitLab champion.
* Original GitLab use-cases not achieved.
* At least two major releases behind.
* Frequently don't show up or are late to scheduled cadence calls.
* Raises more than 15 tickets a month.
* Customer was aquired and parent company uses competitor.
* Negative NPS scores.

~Yellow

* We don’t have much info about how they are using GitLab.
* Don’t have the right contacts for each key person.
* Lack of clearly defined GitLab use-cases. 
* Only one primary stakeholder. 
* At least one major release behind. 
* Doesn't raise any tickets or contribute to any issues.
* Occasionally doesn't show up or are late to scheduled cadence calls.
* Raises more than 10 tickets a month.
* Neutral NPS scores.

~Green

* Successful GitLab use-cases.
* Using the full GitLab DevOps lifecycle.
* Regular communication and positive relationship.
* Healthy user adoption rate.
* Regularly show interest and utilisation in new features.
* At least two successful EBR's every year.
* Always show up to regular cadence calls and communicates when they can't.
* They are interested in upgrading.
* Up to date on releases.
* Raises between 1 and 5 tickets a month and regularly contributes to issues.
* Regularly provides feedback on how to improve GitLab.
* Postive NPS scores.

#### Triage Labels for at risk (~red) accounts

* ~E&A
* ~E&U
* ~U&A
* ~U&U

See [Churn Classification](https://sixteenventures.com/churn-classification) for more info on these labels.

#### Workflow labels

* ~New

Every new issue will have the ~New label. Use the "Colours" board as your main view when viewing issues via an issue board. Once you have reviewed the issue, remove the ~New label.

# Customer Onboarding

We should include a pre-defined set of professional services in each initial purchase. This will address customer experience across all "Gemstone" segmentation of customers whilst setting a precedent to how all customers are managed (with regards to outreach, support, technical account management etc.) throughout the duration of their experience and relationship with GitLab. This also includes small business and mid-market accounts. 

Technical Account Managers and Implementation Engineers should work closely together throughout the onboarding process, with support from Solutions Architects and Strategic Account Leaders/Account Managers where appropriate.

Below is an outline of  the ideal onboarding experiences that we aim to deliver to our customers. These gemstones are NOT intended to replace sales segmentation, they are a way of categorising customers in a way that's relevant to Customer Success.

This includes a Professional Services bundle that would be the most beneficial to the customers in each category based on what we know of historical deals and what they usually need help with. 

## Diamond
Accounts categorized into Diamond are all accounts that are greater than 5000 users, OR greater than 2000 users AND using Ultimate/Gold, OR greater than $500,000 in TAV (Total Account Value).*

| Technical Account Management  | Implementation  |
|---|---|
| Dedicated Technical Account Manager   | Dedicated Implementation Engineer  |
| 60 minute weekly call with TAM and IE  | Hands on keyboard; On-site (total 2 weeks) support throughout onboarding  |
| GitLab Customer Success project  | HA Support with infrastructure-as-code for their HA setup  |
| Personal outreach & automated outreach  | 80 hours remote support throughout the onboarding   |
|   | GitLab Administration Training  |
|   | Up to three additional training courses of their choice, including train-the-trainer certification if desired.  |

## Pearl
Accounts categorized into Pearl are all accounts that are not Diamond AND greater than 2500 users, OR greater than 1000 users AND using Ultimate/Gold, OR greater than $250,000 in TAV (Total Account Value).*

| Technical Account Management  | Implementation  |
|---|---|
| Dedicated Technical Account Manager   | Dedicated Implementation Engineer  |
| 30 minute weekly call with TAM and IE  | On-site (1-3 days) kick-off with architecture discussion and training  |
| GitLab Customer Success project  | 80 hours remote support (across 90 days)  |
| Personal outreach & automated outreach | HA implementation support & consultation  |
|   | Up to two training courses of their choice, including train-the-trainer certification if desired.  |

## Sapphire
Accounts categorised into Sapphire are all accounts that are not Diamond or Pearl AND greater than 1000 users, OR using Ultimate/Premium/Gold/Silver AND greater than $100,000 in TAV (Total Account Value).*

| Technical Account Management  | Implementation  |
|---|---|
| Dedicated Technical Account Manager  | Dedicated Implementation Engineer  |
| 30 minute call every two weeks with TAM and IE  |  40 hours of remote support (across 90 days) |
| GitLab Customer Success project  | HA implementation remote support & consultation  |
| Personal outreach & automated outreach | One training course of their choice  |

## Ruby
Accounts categorised into Sapphire are all accounts that are not Diamond, Pearl or Sapphire AND greater than 500 users, OR greater than $50,000 in TAV (Total Account Value).*

| Technical Account Management  | Implementation  |
|---|---|
| Dedicated Technical Account Manager  (Growth focused - smaller accounts)  | Dedicated Implementation Engineer  |
| Monthly-Weekly TAM calls  | 16 hours of remote support (across 90 days)  |
| GitLab Customer Success project  | No HA setup or training included  |
| Personal outreach & automated outreach  |   |

## Quartz
Accounts categorised into Quartz are all accounts left over after Diamond, Pearl, Sapphire and Ruby have been categorised.

* Account Manager (?)
* Automated outreach
* No Implementation included

**Total Account Value is calculated by adding together the current year’s ARR opportunity and all future renewal opportunity amounts together.*


# Customer Records and Profiling

## The Customer Meta-Record
The concept of a customer meta-record is how a Technical Account Manager develops and maintains a holistic understanding of customer accounts.  It is a living record that includes both business and technical information about the customer.  The data captured here informs not only the Technical Account Manager, but all of GitLab about critical details to the success of our customers. 

## Business Profile
The business profile is captured during the discovery and scoping stages in the pre-sales phase of the customner lifecycle.  Once an opportunity is set to closed-won, the Technical Account Manager is largely responsible for maintaining this data. The business profile of a customer's meta-record contains data points related to their organizational structure, the internal advocate/champion, the features most compelling to the customer, the "why" of purchasing GitLab, objectives for the implementation and critically, the data captured in the customer feedback loop.

## Technical Profile
The technical profile includes objective data points about a customers technical landscape and can be captured at any stage of the customer lifecycle. Specifically, items related to architecture, sizing, scale, security and compliance requirements are captured in the technical profile. In general, solution architects and implementation specialists are primarily responsible for collecting and capturing information for the technical profile. This information is transitioned to the Technical Account Manager once a customer goes live and the Technical Account Manager maintains the information throughout the remainder of the customer lifecycle.

## Automated Customer Discovery Tools
A series of data sheets and automation scripts is being developed that will streamline much of the intake, discovery and reconnaissance activities during the pre-sales stages of the customer lifecycle.  Decisions surrounding where this data will reside and which information is part of the critical path during customer onboarding are currently underway.

Currently, These profiles and data points are a work in progress and the Customer Success Department are trying and testing how to efficiently do this using various different tools and methods. 

Some of these currently include:  

* The Customer Collaboration Project - we are testing this as a type of customer success plan as well as a collaboration project between the team assigned to the customer.
* Google documents, such as decks (a slightly older method) which outline a success plan created by the Strategic Account Leader and running notes pages within "Pods", where account planning is reviewed each week/bi-weekly between those pods.
* Salesforce (SFDC): This is our main source of truth and contains all activity, notes and communication between Technical Account Managers, Solutions Architects and Implementation Engineers and their respective customers. The Customer Success Plans should also be linked to this record.

We are currently researching various Customer Success Applications, such as Gainsight, with which to fully manage post-sales customer success, reporting and strategy around accounts. Salesforce is somewhat limited in certain areas that pertain to Customer Success so this is becoming more of a necessity and will hopefully become part of our workflow some time in 2019.
