var CategoryRoiCalculator = {
  selector: '.js-roi-calculator',
  init: function() {
    if ($(this.selector).length > 0) {
      this.setup();
    }
  },
  setup: function() {
    this.$inputs = $(this.selector).find('.js-roi-calculator-input');
    this.$total = $(this.selector).find('#js-roi-calculator-total');
    this.$inputs.on('change', this.handleChange.bind(this));
  },
  handleChange: function() {
    this.updateTotal();
  },
  updateTotal: function() {
    this.$total.html('$' + this.calculateTotal());
  },
  calculateTotal: function() {
    var total = 0;
    this.$inputs.each(function() {
      var cost = Number.parseFloat($(this).val());
      total += Number.isNaN(cost) ? 0 : cost;
    });
    return total;
  }
};

$(CategoryRoiCalculator.init.bind(CategoryRoiCalculator));
