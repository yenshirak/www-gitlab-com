require 'yaml'

module Gitlab
  module Homepage
    class Category
      attr_reader :key

      def initialize(key, data)
        @key = key
        @data = data
      end

      ##
      # Middeman Data File objects compatibiltiy
      #
      def method_missing(name, *args, &block) # rubocop:disable Style/MethodMissing
        @data[name.to_s]
      end

      def self.all!
        @categories ||= YAML.load_file('data/categories.yml')
        @categories.map do |key, data|
          new(key, data)
        end
      end

      def self.for_stage(stage)
        all!.dup.keep_if { |category| category.stage == stage.key }
      end
    end
  end
end
